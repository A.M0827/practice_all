const str =
  "9On the weekend, we went to the park and enjoyed playing badminton. We had sandwiches which I made and coffee for lunch. Then, we threw ourselves down on the grass and read books.";
// const specialChars = `/,|\.|@|!|\?|[0-9]/g`;
const ary = str.replace(/,|\.|@|!|\?|\d/gi, "").split(" ");
// const ary = str.replace(/,|\./g, "").split(" ");
// const ary = str.replace(new RegExp(specialChars,"g"),"").split(" ");
// console.log(ary);

const newAry = ary.map((v) => ({
  ["word"]: v.toLowerCase(),
  ["cnt"]: ary.filter((x) => x.toLowerCase() === v.toLowerCase()).length,
}));
// console.log(newAry);

// 同じ単語削除
const ranking = newAry.filter((obj1, index, array) => {
  return array.findIndex((obj2) => obj1.word === obj2.word) === index;
});

ranking.sort((a, b) => b.cnt - a.cnt);
ranking.sort((a, b) => {
  if (a.cnt !== b.cnt) {
    return;
  }
  if (a.word > b.word) return 1;
  if (a.word < b.word) return -1;
  return 0;
});

console.log(ranking);

// const test = ary.reduce((prev, current) => {
//     prev[current] = (prev[current] || 0) + 1;
//     return prev;
// },{})
// console.log(test);
